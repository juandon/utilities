# Utilities
Arrays related utilities.
Extended parse_ini_string() / parse_ini_file().

Installing:
---
Add following code to your "composer.json" file
```json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/juandon/utilities"
        }
    ],
    "require": {
        "juandon/utilities": "dev-develop"
    }
```
and run `composer update`.
