<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Utilities
 * @subpackage Config
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Utilities\Config;

/**
 * Extended parse_ini_string() / parse_ini_file().
 *
 * <code>Ini::parseFile("./ini.ini", TRUE);</code> using following ini-file
 * <code>
 * [core]
 * log[method] = "StdOut"
 * log.source[] = 'Foo::method1()'
 * log.source[] = 'Foo::method2()'
 * log.source.2 = 'Foo::method3()'
 * </code>
 * will produce
 * <code>
 * Array (
 *     [core] => Array (
 *         [log] => Array (
 *             [method] => StdOut
 *             [source] => Array (
 *                 [0] => Foo::method1()
 *                 [1] => Foo::method2()
 *                 [2] => Foo::method3()
 *             )
 *         )
 *     )
 * )
 * </code>
 *
 * @package    Utilities
 * @subpackage Config
 * @static
 */
class Ini
{
    /**
     * @var string
     */
    protected static $delimiter = '.';

    /**
     * @param  string $delimiter
     * @return void
     */
    public static function setSectionDelimiter($delimiter)
    {
        self::$delimiter = $delimiter;
    }

    /**
     * Parse a configuration string.
     *
     * @param  string $string
     * @param  bool   $processSections
     * @param  int    $scannerMode
     * @return mixed  Array or FALSE
     * @link   http://php.net/manual/en/function.parse-ini-string.php
     */
    public static function parseString($string, $processSections = FALSE, $scannerMode = INI_SCANNER_NORMAL)
    {
        $parsed = parse_ini_string($string, $processSections, $scannerMode);
        self::parse($parsed, $processSections);

        return $parsed;
    }

    /**
     * Parse a configuration file.
     *
     * @param  string $path
     * @param  bool   $processSections
     * @param  int    $scannerMode
     * @return mixed  Array or FALSE
     * @link   http://php.net/manual/en/function.parse-ini-file.php
     */
    public static function parseFile($path, $processSections = FALSE, $scannerMode = INI_SCANNER_NORMAL)
    {
        $parsed = parse_ini_file($path, $processSections, $scannerMode);
        self::parse($parsed, $processSections);

        return $parsed;
    }

    /**
     * @param  mixed  &$parsed
     * @param  bool   $processSections
     * @return void
     */
    protected static function parse(&$parsed, $processSections)
    {
        if (is_array($parsed)) {
            if ($processSections) {
                foreach (array_keys($parsed) as $section) {
                    self::parseSection($parsed[$section]);
                }
            } else {
                self::parseSection($parsed);
            }
        }
    }

    /**
     * @param  mixed  &$parsed
     * @return void
     */
    protected static function parseSection(&$parsed)
    {
        $sections = array_keys($parsed);
        foreach ($sections as $section) {
            if (FALSE !== strpos($section, self::$delimiter)) {
                $deeperSections = explode(self::$delimiter, $section);
                $nextSection = $deeperSections[0];
                $value = $parsed[$section];
                unset($parsed[$section]);
                if (isset($parsed[$nextSection]) ? !is_array($parsed[$nextSection]) : TRUE) {
                    $parsed[$nextSection] = [];
                }
                self::setSections($parsed, $deeperSections, $value);
            }
        }
    }

    /**
     * @param  array &$target
     * @param  array $deeperSections
     * @param  mixed $value
     * @return void
     */
    protected static function setSections(&$target, $deeperSections, $value)
    {
        $section = $deeperSections[0];
        if (sizeof($deeperSections) > 1) {
            if (!isset($target[$section])) {
                $target[$section] = [];
            }
            self::setSections(
                $target[$section],
                array_slice($deeperSections, 1),
                $value
            );
        } else {
            if ('[]' == substr($section, -2)) {
                $section = substr($section, 0, -2);
                if (!isset($target[$section])) {
                    $target[$section] = [];
                }
                $target[$section][] = $value;
            }
            $target[$section] = $value;
        }
    }
}
